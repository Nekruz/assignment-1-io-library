 section .text
%define SYS_EXIT 60
%define NULL_TERMINATOR 0
%define FD_STDOUT 1
%define NEW_SYRING 10
%define SPACE 0x20
%define TAB 0x9
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT
    syscall
     


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax,rax
            .loop:
            cmp byte[rdi+rax], NULL_TERMINATOR  ; Проверить является ли текущий символ нуль-термирование
            je .end
            inc rax
            jmp .loop
        .end:   
            ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax
        mov rsi, rdi        ; Указать на адрес 
        call string_length  
        mov rdx, rax        
        mov rdi, FD_STDOUT         
        mov rax, 1          ; 'write' syscall number
        syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax,rax
        push rdi            
        mov rsi, rsp        ; Указать на адрес символа вывода
        pop rdi             
        mov rax, 1          ; 'write' syscall number
        mov rdx, 1         
        mov rdi, FD_STDOUT        
        syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEW_SYRING        ; Указать на символ перевода строки 
    push rcx            
    call print_char     ; Вывести символ 0xA
    pop rcx
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
         mov r8, 1             ; счётчик
        xor rdx, rdx 
        mov rax, rdi
        mov r9, 10                     
        dec rsp               
        mov byte [rsp], NULL_TERMINATOR     ; Поставить конец строки в стеке
        .loop:                  
            div r9            ; Делить число на 10, чтобы взять последнее число
            mov rsi, rdx      ; Остаток от деление это значить последнее число. Будем записать на rsi
            inc r8
            add rsi, 48       
            xor rdx, rdx         
            dec rsp           ; Для выделение  байт для число
            mov [rsp], sil          
            test rax,rax        ; Проверить последнее ли число
            jnz .loop         
        mov rdi, rsp          ; Записать результат в rdi
        push rcx              
        push rsi
        call print_string     ; Выводим результат
        pop rsi
        pop rcx
        add rsp, r8           ; Вернуть стек в оргинальное состояние
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rdx,rdx
        xor rax, rax
        mov rax, rdi
        cmp rax, 0          ; Если число > 0 то сразу выводим
        jge .print          ; Если нет то выводим с минусом положительное число
        push rax
        push rdx
        push rcx
        mov rdi, '-'
        call print_char
        pop rcx
        pop rdx
        pop rax
        neg rax
        .print:
            mov rdi, rax
            push rcx
            call print_uint
            pop rcx
            ret
    


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov rax, 1
        mov r9, rsi
        mov r8, rdi                                  
        xor rcx, rcx
        xor rsi, rsi
        xor rdi, rdi
        
        .loop:
            mov dil, [r8+rcx]
            mov sil, [r9+rcx]
            cmp sil, dil
            jne .NotEqual
            cmp byte dil, 0         
            je .end
            inc rcx
            jmp .loop
        .NotEqual:
            xor rax,rax
        .end:
            ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
        mov rdx, 1          ; Указать на длину строки ввода
        xor rdi,rdi         ; Указать на stdin
        xor rax,rax          
        dec rsp
        mov rsi, rsp
        syscall
        test rax,rax
        jz .zero
        mov rax, [rsp]
        inc rsp
    ret 
        .zero:
            xor rax, rax
            inc rsp
            ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi
    mov r9, rsi

    .skip_whitespaces:          ; Удаление предыдущих пробелов
        call read_char
        cmp al, SPACE
        je .skip_whitespaces
        cmp al, TAB
        je .skip_whitespaces
        cmp al, NEW_SYRING
    xor rdx, rdx                
    jmp .write
    .loop:                      ; Цикл для ввода символа
        push rdx
        call read_char
        pop rdx
    .write:                     
        cmp al, NEW_SYRING            
        je .end
        cmp al, SPACE
        je .end
        cmp al, 4
        je .end 
        cmp al, TAB
        je .end
        test al, al
        je .end
        inc rdx
        cmp rdx, r9
        jge .overflow
        dec rdx
        mov [r8+rdx], al
        inc rdx
        jmp .loop
    .end:                       ; Сохраняем результат в rax
        mov byte[r8+rdx],NULL_TERMINATOR
        mov rax, r8
        ret
    .overflow:                  
        xor rax, rax
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось


parse_uint:
    xor r8,r8
    xor r9,r9
    mov r10, 10
     xor rax,rax
    .loop: 
        mov r8b, [rdi + r9]
        cmp r8b, '0'
        jl .end
        cmp r8b, '9'
        jg .end                     
        sub r8b, '0'                ; Перевод из ASCII в число
        mul r10                     
        add rax, r8                 
        inc r9                      ; инкрементируем длину числа
        jmp .loop
    .end:
        mov rdx, r9
        ret
    ret
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'      ; Проверяем отрицательное ли число
    jne parse_uint          ; Если нет передаём к фукцию parse_uint
    inc rdi                 
    call parse_uint         ; Если отрицателен то парсим положительное число
    neg rax                 
    test rdx,rdx              
    je .end
    inc rdx                 ; Добавляем минус в длину строки
    .end:
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx,rcx
    xor r9,r9
    push rdi
    call string_length              ; Узнаем длину строки
    pop rdi
    cmp rax, rdx                    ; Если длина строки меньше чем длины буфера
    jle .loop                       ; то идем в цикл
    xor rax,rax                     ; если больше то просто остановим работу
    ret
     .loop:                          
        mov r9b, [rdi+rcx]
        mov [rsi +rcx],r9b
        inc rcx
        cmp byte[rsi + rcx],NULL_TERMINATOR
        jne .loop
    mov rax, rdx
    ret
